import React, { Component } from "react";
import "./index.css";
import todosList from "./todos.json";
import { Route, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import TodoList from "./TodoList";
import { addTodo, clearCompletedTodos } from "./actions";

class App extends Component {
  state = {
    todos: todosList,
    value: ""
  };

handleDelete = todoIdToDelete => {
  const newTodoList = this.state.todos.filter(
    todo => todo.id !== todoIdToDelete
  );
    this.setState({ todos: newTodoList });
};

handleCreate = event => {
  if (event.key === "Enter") {
    this.props.addTodo(this.state.value);
    this.setState({ value: "" });
  }
};



handleClearCompleted = () => {
    const newTodoList = this.state.todos.filter(todo => {
      if (todo.completed === true) {
        return false;
      }
      return true;
    });
    this.setState({ todos: newTodoList })
  };

  handleToggle = todoIdToToggle => {
    this.props.toggleTodo(todoIdToToggle);
  }; 

handleChange = (event) => {
  this.setState({ value: event.target.value });
};

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus
            onKeyDown={this.handleCreate}
            onChange={this.handleChange}
            value={this.state.value}
          />
        </header>
        <Route 
          exact
          path="/"
          render={() => (
          <TodoList  
            handleDelete={this.handleDelete} 
            todos={this.props.todos}
            /> 
          )}
        />
        <Route 
          path="/active"
          render={() => (
          <TodoList
            handleDelete={this.handleDelete} 
            todos={this.props.todos.filter(todo =>
            todo.completed === false)}
            /> 
          )}
        />
        <Route 
          path="/completed"
          render={() => (
          <TodoList 
            handleDelete={this.handleDelete} 
            todos={this.props.todos.filter(todo =>
            todo.completed === true)}
            /> 
          )}
        />
        <footer className="footer">
  {/* <!-- This should be `0 items left` by default --> */}
  <span className="todo-count">
          <strong>{this.state.todos.filter(todo => todo.completed === false).length}</strong> item(s) left
  </span>
  <ul className="filters">
    <li>
      <NavLink exact to="/" activeClassName="selected">
        All
        </NavLink>
    </li>
    <li>
      <NavLink to="/active">Active</NavLink>
    </li>
    <li>
      <NavLink to="/completed">Completed</NavLink>
    </li>
  </ul>
  <button 
    className="clear-completed" 
    onClick={this.props.clearCompletedTodos}
  >
    Clear completed
 </button>
</footer>
</section>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    todos: state.todos
  };
};

const mapDispatchToProps = {
  addTodo,
  clearCompletedTodos   
};

export default connect(
  mapStateToProps, 
  mapDispatchToProps
)(App);
